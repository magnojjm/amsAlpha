# -*- coding: utf-8 -*-
{
    "name": "Protect ir.rule records",
    "vesion": "10.0.1.0.0",
    "author": "IT-Projects LLC, Ivan Yelizariev",
    "category": "Access",
    "support": "apps@itpp.dev",
    "website": "https://twitter.com/yelizariev",
    "license": "Other OSI approved licence",  # MIT
    "depends": [],
    "data": ["views.xml"],
    "installable": True,
}
