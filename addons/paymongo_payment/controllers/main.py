# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request


class PayMongoController(http.Controller):
    @http.route(['/payment/paymongo/s2s/create_json_3ds'], type='json', auth='public', csrf=False)
    def paymongo_s2s_create_json_3ds(self, verify_validity=False, **kwargs):
        if not kwargs.get('partner_id'):
            kwargs = dict(kwargs, partner_id=request.env.user.partner_id.id)
        token = False
        error = None

        try:
            token = request.env['payment.acquirer'].browse(int(kwargs.get('acquirer_id'))).s2s_process(kwargs)
        except Exception as e:
            error = str(e)

        if not token:
            res = {
                'result': False,
                'error': error,
            }
            return res

        res = {
            'result': True,
            'id': token.id,
            'short_name': token.short_name,
            '3d_secure': False,
            'verified': True,
        }
        # TODO: Need to create verification of cards
        return res
