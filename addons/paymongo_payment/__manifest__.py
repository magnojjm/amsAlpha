# -*- coding: utf-8 -*-

{
    'name': 'PayMongo Payment Acquirer',
    'category': 'Accounting/Payment',
    'summary': 'Payment Acquirer: PayMongo Implementation',
    'version': '13.0.1',
    'author': 'AMC | Private Company',
    'description': """PayMongo Payment Acquirer""",
    'depends': ['payment', 'sale', 'sale_management', 'account'],
    'data': [
        'views/payment_views.xml',
        'views/payment_paymongo_templates.xml',
        'views/sales_inherit_views.xml',
        'data/payment_acquirer_data.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'post_init_hook': 'create_missing_journal_for_acquirers',
    'uninstall_hook': 'uninstall_hook',
}
