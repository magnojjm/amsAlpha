# List of Errors for the PayMongo Payment Acquirer
'''
    ER-100 = Form Payment Related
    ER-101 = PaymentToken Related

    ER-102 = Payment Attach Related
        ER-102A = if data not in response

    ER-103 = Payment Intent Related
        ER-103A = general intent error
        ER-103B = if the intent response has error

    ER-104 = Payment Method Related
        ER-104A = attribute error

    ER-105 = Payment Gcash Related

    ER-107 = Payment List
'''