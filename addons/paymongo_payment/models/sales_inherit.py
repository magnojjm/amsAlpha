# -*- coding: utf-8 -*-
from odoo import models, fields, api


class PaymongoPaymentList(models.Model):
    _name = 'pm.payment.list'

    company = fields.Char(string='Company')
    payment_method = fields.Char(string='Payment Method')
    amount = fields.Float(string='Amount')
    status = fields.Char(string='Status')
    customer = fields.Char(string='Customer')
    email = fields.Char(string='Email')
    contact = fields.Char(string='Contact')
    reference = fields.Char(string='Reference')
    is_current_company = fields.Boolean(string='Is current company', default=False, compute='get_current_company')
    display = fields.Char(string='Display')

    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, 'PM000%s' % (record.id)))
        return result

    # @api.model
    # def _fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):
    #     result = super(PaymongoPaymentList, self)._fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
    #     if view_type == 'tree':
    #         self.create_domain_in_tree_view()
    #         self.get_current_company()
    #     return result

    @api.depends('company')
    def get_current_company(self):
        # print('get_current_company')

        for rec in self:
            if rec.company == self.env.user.company_id.name:
                rec.is_current_company = True
                rec.display = 'Yes'
            else:
                rec.is_current_company = False
                rec.display = 'No'
