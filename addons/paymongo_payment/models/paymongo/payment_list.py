import requests
import json
from .urls import payment_list_url
from .constants import headers
import pprint


class PaymentList(object):
    def __init__(self, secret_key):
        self.secret_key = secret_key

    def retrieve(self, key):
        querystring = {"limit": "10"}
        response = requests.get(payment_list_url, headers=headers, auth=(self.secret_key, ''), params=querystring)
        return response.json()
