# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError

import time
from .paymongo import Paymongo


class PaymentAcquirerPayMongo(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('paymongo', 'PayMongo')])
    pm_secret_key_id = fields.Char(string='Secret Key ID: ',
                                   required_if_provider='paymongo',
                                   groups='base.group_user')
    max_amount = fields.Float(string='Max Amount Per Transaction: ',
                              required_if_provider='paymongo',
                              groups='base.group_user',
                              default='1000000.00',
                              required=True)

    # Constants & Local Variables
    email_to_contact = 'admin_support@company.com'
    # TODO: Create hash for the values
    values = {}

    def _get_feature_support(self):
        res = super(PaymentAcquirerPayMongo, self)._get_feature_support()
        res['tokenize'].append('paymongo')
        return res

    def paymongo_s2s_form_validate(self, data):
        error = dict()
        mandatory_fields = ["cc_number", "cc_cvc", "cc_holder_name", "cc_expiry", "cc_brand"]
        for field_name in mandatory_fields:
            if not data.get(field_name):
                error[field_name] = 'missing'
        return False if error else True

    def paymongo_s2s_form_process(self, data):
        try:
            values = {
                'cc_number': data.get('cc_number'),
                'cc_cvc': int(data.get('cc_cvc')),
                'cc_holder_name': data.get('cc_holder_name'),
                'cc_expiry': data.get('cc_expiry'),
                'cc_brand': data.get('cc_brand'),
                'acquirer_id': int(data.get('acquirer_id')),
                'partner_id': int(data.get('partner_id'))
            }
            self.values.update(values)
            return self.env['payment.token'].sudo().create(values)  # return the pm_id
        except:
            raise ValidationError('Internal Error Found: Please contact us through {em}. <ER-100>') \
                .format(em=self.email_to_contact)


class PaymentTxPayMongo(models.Model):
    _inherit = 'payment.transaction'
    # payments = []
    references = []

    def display_error_msg(self, error):
        error_msg = 'Sorry we found an error during the payment process.\n' \
                    ' Please contact us through {em}.\n{err}'.format(em=self.acquirer_id.email_to_contact, err=error)
        return error_msg

    def display_payment(self, secret_key):
        try:
            paymongo_app = Paymongo(secret_key)
            res = paymongo_app.payment_list.retrieve(secret_key)

            # check if reference is not yet on the record list
            payments = self.env['pm.payment.list'].search([])
            for payment in payments:
                if payment.reference not in self.references:
                    self.references.append(payment.reference)

            # display payment
            for current_payment in res['data']:
                payment_attr = current_payment['attributes']
                desc = current_payment['attributes'].get('description')

                if eval(desc)['reference'] not in self.references:
                    converted_amount = format(int(str(payment_attr.get('amount'))[:-2]), '.2f')
                    payment_method = payment_attr.get('source')['type']

                    # check payment method
                    if payment_method == 'card':
                        method = 'Credit or Debit Card'
                    elif payment_method == 'gcash':
                        method = 'GCash'
                    elif payment_method == 'grab_pay':
                        method = 'GrabPay'
                    else:
                        method = payment_method

                    vals = {
                        'company': eval(desc)['company'],
                        'payment_method': method,
                        'amount': converted_amount,
                        'status': payment_attr.get('status'),
                        'customer': eval(desc)['customer'],
                        'email': eval(desc)['customer_email'],
                        'contact': eval(desc)['customer_contact'],
                        'reference': eval(desc)['reference'],
                    }
                    self.env['pm.payment.list'].create(vals)
        except:
            raise ValidationError(self.display_error_msg('<ER-107>'))

    def get_description(self):
        desc = {
            'company': self.env.user.company_id.name,
            'customer': self.partner_name,
            'customer_email': self.partner_email,
            'customer_contact': self.partner_phone,
            'reference': self.reference,
        }
        return desc

    def paymongo_create_payment_intent(self, secret_key):
        # PayMongo only supports PHP
        if self.currency_id.name != 'PHP':
            raise ValidationError('Sorry, Currency ({cur}) is not yet supported by our system.'
                                  .format(cur=self.currency_id.name))

        # limit maximum allowable amount per transaction
        if self.amount > self.acquirer_id.max_amount:
            raise ValidationError('The value for amount cannot'
                                  ' be greater than {value:,.2f}'.format(value=float(self.acquirer_id.max_amount)))
        else:
            try:
                paymongo_app = Paymongo(secret_key)
                payment_intent_payload = {
                    'data': {
                        'attributes': {
                            'amount': int(self.amount * 100),
                            'payment_method_allowed': ['card'],
                            'description': '{details}'.format(details=self.get_description()),
                            'statement_descriptor': 'statement descriptor',
                            'payment_method_options': {
                                'card': {'request_three_d_secure': 'automatic'}
                            },
                            'currency': 'PHP',
                        }
                    }
                }
                intent_response = paymongo_app.payment_intents.create(payment_intent_payload)
            except:
                raise ValidationError(self.display_error_msg('<ER-103>'))

            if 'data' in intent_response.keys():
                return intent_response['data']
            elif 'errors' in intent_response.keys():
                if intent_response['errors'][0]['code'] == 'parameter_below_minimum':
                    raise ValidationError('Sorry, the minimum order should not be less than 100.00')
                else:
                    raise ValidationError(self.display_error_msg('<ER-103B>'))
            else:
                raise ValidationError(self.display_error_msg('<ER-103A>'))

    def paymongo_create_payment_method(self, secret_key, data):
        try:
            paymongo_app = Paymongo(secret_key)
            payment_method_payload = {
                'data': {
                    'attributes': {
                        'type': 'card',
                        'details': {
                            'card_number': str(data.get('cc_number').replace(' ', '')),
                            'exp_month': int(data.get('cc_expiry').split('/')[0]),
                            'exp_year': int(data.get('cc_expiry').split('/')[1]),
                            'cvc': str(data.get('cc_cvc')),
                        }
                    }
                }
            }
            method_response = paymongo_app.payment_methods.create(payment_method_payload)
            if 'data' in method_response.keys():
                return method_response['data']
            else:
                raise ValidationError(self.display_error_msg('<ER-104A>'))
        except:
            raise ValidationError(self.display_error_msg('<ER-104>'))

    def paymongo_s2s_do_transaction(self, **kwargs):
        self.ensure_one()
        secret_key = self.acquirer_id.pm_secret_key_id
        data = self.acquirer_id.values

        # Call PayMongo API
        intent_id = self.paymongo_create_payment_intent(secret_key)['id']
        method_id = self.paymongo_create_payment_method(secret_key, data)['id']

        try:
            paymongo_app = Paymongo(secret_key)
            payload = {
                'data': {
                    'attributes': {
                        'client_key': 'card',
                        'payment_method': method_id,
                    }
                }
            }
            attach_response = paymongo_app.payment_intents.attach(intent_id, payload)
        except:
            raise ValidationError(self.display_error_msg('<ER-102>'))

        if 'data' in attach_response.keys():
            status = attach_response['data']['attributes']['status']
            if status == 'succeeded':
                '''
                    self._set_transaction_done() - payment is already paid / posted (Orders)
                    self._set_transaction_pending() - payment is already paid / for confirmation (Unpaid Orders)
                    self._set_transaction_cancel() - payment is cancelled
                '''
                self._set_transaction_done()
                self.display_payment(secret_key)
                return True
            else:
                self._set_transaction_cancel()
                return False
        else:
            raise ValidationError(self.display_error_msg('<ER-102A>'))


class PaymentToken(models.Model):
    _inherit = 'payment.token'

    def paymongo_create(self, values):
        try:
            if values.get('cc_number'):
                values['cc_number'] = values['cc_number'].replace(' ', '')
                alias = 'ODOO-NEW-ALIAS-%s' % time.time()
                return {
                    'acquirer_ref': alias,
                    'name': 'XXXXXXXXXXXX%s - %s' % (values['cc_number'][-4:], values['cc_holder_name'])
                }
            return {}
        except:
            raise Exception('Internal Error Found: Please contact us through {em}. <ER-101>'
                            .format(em=self.acquirer_id.email_to_contact))

