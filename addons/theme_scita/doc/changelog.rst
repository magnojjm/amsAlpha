========================
``Theme Scita`` changelog
========================

******************************************
Fixed an issue related to product paging
******************************************

1. Fixed an issue related to product paging


******************************************
Removed unwanted library and Added Pre made industry demo
******************************************

1. Removed unwanted jquery library due to which the issue in mobile has occured.
2. For dynamic sliders only published products will be available to be added.
3. Pre-made industry demo's will be available


*************************
Update For multi website
*************************

1. Pre-made industry demo's will be available for multi-website also.


*************************
Added new features and Christmas Ecommerce industry
*************************

1. Added new features and Christmas Ecommerce industry


*************************
Added new industry demo
*************************

1. Added new industry demo for Printing Media


*************************
Fixed a responsive issue
*************************

1. Fixed a responsive issue


*************************
Added New Snippets and Fixed some responsive issues
*************************

1. Added New Category Slider Snippets
2. Fixed some issue related to responsive

